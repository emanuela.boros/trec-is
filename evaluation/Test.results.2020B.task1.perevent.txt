TREC-IS 2020B Task 1 Notebook Evaluator v2.7
Run: Test (../../../DATA/TREC-IS/Task-A-2021-data-processed/test.results.0.json)

--------------------------------------------------
EVALUATON: Information Type Categorization (Multi-type)
Per Event Performance
--------------------------------------------------
TRECIS-CTIT-H-060
  Information Type Precision (positive class, multi-type, macro): 0.041666666666666664
  Information Type Recall (positive class, multi-type, macro): 0.001730343300110742
  Information Type F1 (positive class, multi-type, macro): 0.003322700691121743
  Information Type Accuracy (overall, multi-type, macro): 0.001730343300110742

TRECIS-CTIT-H-061
  Information Type Precision (positive class, multi-type, macro): 0.20691920786394746
  Information Type Recall (positive class, multi-type, macro): 0.14669428798929227
  Information Type F1 (positive class, multi-type, macro): 0.14115939313482687
  Information Type Accuracy (overall, multi-type, macro): 0.7845862699034193

TRECIS-CTIT-H-062
  Information Type Precision (positive class, multi-type, macro): 0.15053826026672112
  Information Type Recall (positive class, multi-type, macro): 0.13298276602180134
  Information Type F1 (positive class, multi-type, macro): 0.11743158100876465
  Information Type Accuracy (overall, multi-type, macro): 0.46516666666666656

TRECIS-CTIT-H-063
  Information Type Precision (positive class, multi-type, macro): 0.22889467771830008
  Information Type Recall (positive class, multi-type, macro): 0.09441148526501074
  Information Type F1 (positive class, multi-type, macro): 0.11075156508541878
  Information Type Accuracy (overall, multi-type, macro): 0.7963781848810275

TRECIS-CTIT-H-064
  Information Type Precision (positive class, multi-type, macro): 0.08776159276044208
  Information Type Recall (positive class, multi-type, macro): 0.05053274505171292
  Information Type F1 (positive class, multi-type, macro): 0.039547126537830046
  Information Type Accuracy (overall, multi-type, macro): 0.5548702133127731

TRECIS-CTIT-H-065
  Information Type Precision (positive class, multi-type, macro): 0.1881235208220264
  Information Type Recall (positive class, multi-type, macro): 0.10109042120593516
  Information Type F1 (positive class, multi-type, macro): 0.11600866763749623
  Information Type Accuracy (overall, multi-type, macro): 0.5723333333333334

TRECIS-CTIT-H-066
  Information Type Precision (positive class, multi-type, macro): 0.15471478908074607
  Information Type Recall (positive class, multi-type, macro): 0.06805145545012382
  Information Type F1 (positive class, multi-type, macro): 0.07741701849377335
  Information Type Accuracy (overall, multi-type, macro): 0.7569305277221109

TRECIS-CTIT-H-068
  Information Type Precision (positive class, multi-type, macro): 0.062229848122619
  Information Type Recall (positive class, multi-type, macro): 0.036983764573546934
  Information Type F1 (positive class, multi-type, macro): 0.027579399089031623
  Information Type Accuracy (overall, multi-type, macro): 0.6991792929292929

TRECIS-CTIT-H-069
  Information Type Precision (positive class, multi-type, macro): 0.09468529504738572
  Information Type Recall (positive class, multi-type, macro): 0.04533740585245464
  Information Type F1 (positive class, multi-type, macro): 0.05520277672131799
  Information Type Accuracy (overall, multi-type, macro): 0.6796469366562826

TRECIS-CTIT-H-070
  Information Type Precision (positive class, multi-type, macro): 0.12477436721320116
  Information Type Recall (positive class, multi-type, macro): 0.04872363660868303
  Information Type F1 (positive class, multi-type, macro): 0.051418448173495966
  Information Type Accuracy (overall, multi-type, macro): 0.7254137798306389

TRECIS-CTIT-H-071
  Information Type Precision (positive class, multi-type, macro): 0.04262502298216583
  Information Type Recall (positive class, multi-type, macro): 0.024049598647986933
  Information Type F1 (positive class, multi-type, macro): 0.02581441120141677
  Information Type Accuracy (overall, multi-type, macro): 0.6109954908081859

TRECIS-CTIT-H-073
  Information Type Precision (positive class, multi-type, macro): 0.04378989675042307
  Information Type Recall (positive class, multi-type, macro): 0.024082319791535352
  Information Type F1 (positive class, multi-type, macro): 0.02205168766032889
  Information Type Accuracy (overall, multi-type, macro): 0.5431794605809128

TRECIS-CTIT-H-074
  Information Type Precision (positive class, multi-type, macro): 0.0842851826405588
  Information Type Recall (positive class, multi-type, macro): 0.022255344090702404
  Information Type F1 (positive class, multi-type, macro): 0.019388772898748192
  Information Type Accuracy (overall, multi-type, macro): 0.6190725244072526

TRECIS-CTIT-H-075
  Information Type Precision (positive class, multi-type, macro): 0.049810039860488285
  Information Type Recall (positive class, multi-type, macro): 0.02642014351467824
  Information Type F1 (positive class, multi-type, macro): 0.027177845708547466
  Information Type Accuracy (overall, multi-type, macro): 0.669543904518329


