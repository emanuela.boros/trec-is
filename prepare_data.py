# -*- coding: utf-8 -*-
import json
import os
import pandas as pd
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--labels', type=str, default='../DATA/TREC-IS/Task-A-2021/TRECIS-2018-2020B.json',
                    help='The path of the labels file (TRECIS-2018-2020B.json)')
parser.add_argument('--topics', type=str, default='../DATA/TREC-IS/Task-A-2021/TRECIS-2018-2020B.topics',
                    help='The path of the topics file (TRECIS-2018-2020B.topics)')
parser.add_argument('--jsons_train', type=str, default='../DATA/TREC-IS/Task-A-2021-data',
                    help='The path of the directory containing the jsons')
parser.add_argument('--jsons_test', type=str, default='../DATA/TREC-IS/Task-A-2021-data-extra',
                    help='The path of the directory containing the jsons')
parser.add_argument('--out', type=str, default='../DATA/TREC-IS/Task-A-2021-data-processed',
                    help='The path of the directory where the *.csv data will be saved')
parser.add_argument("--train",
                    action='store_true',
                    help="Whether to generate data for training.")
parser.add_argument("--test",
                    action='store_true',
                    help="Whether to generate data for testing (competition).")

args = parser.parse_args()
labels_file = args.labels
topics_file = args.topics
data_dir = args.jsons_train
data_test_dir = args.jsons_test
out_dir = args.out
train = args.train
test = args.test


def change_num(x):
    if x in ['53', '54', '55', '56', '57', '58', '59', '60', '61', '62', '63', '64', '65', '66', '67', '68',
             '69', '70', '71', '72', '73', '74', '75']:  # Some json files are incorrect 53 --> TRECIS-CTIT-H-053
        x = 'TRECIS-CTIT-H-0' + x
    # We remove the "Training" because in the labels file there are only
    # training instances
    x = x.replace('-Training', '')
    # We filter them considering the labels file
    x = x.replace('-Test', '')
    return x


if __name__ == '__main__':
    if train:
        with open(labels_file, 'r') as f:
            # The file that contains the labels, this file
            # will be mapped with the topics file in order
            # to take the correct name of the json fireColorado2012 -->
            # TRECIS-CTIT-H-001
            labels_task1 = json.load(f)

        with open(topics_file, 'r') as f:
            topics = f.readlines()

        # Reading the XML with topics
        nums = [x.replace('<num>', '').replace('</num>', '').strip()
                for x in topics if '<num>' in x]
        nums = [change_num(x) for x in nums]
        datasets = [
            x.replace(
                '<dataset>',
                '').replace(
                '</dataset>',
                '').strip() for x in topics if '<dataset>' in x]

        print('Datasets:', nums)
        print('Datasets events:', datasets)

        # Reading the jsons
        JSON_DATA = {}
        for path, directories, files in os.walk(data_dir):
            for file in files:
                if file.endswith('json'):
                    print('Reading %s' % os.path.join(path, file))
                    title = file.split('.')[-2]
                    if title not in JSON_DATA:
                        JSON_DATA[title] = []
                    with open(os.path.join(path, file), 'r') as f:
                        tweets = []
                        for line in f.readlines():
                            tweets.append(json.loads(line))
                        JSON_DATA[title] += tweets

        TWEET_DATA = []
        missing = []
        present = []
        unique_labels = []

        not_in_train = []

        for data in labels_task1['events']:

            for tweet in data['tweets']:
                eventID = tweet['eventID']

                if eventID not in datasets:
                    not_in_train.append(eventID)
                else:
                    dataset_index = datasets.index(eventID)
                    eventID = nums[dataset_index]

                    postID = tweet['postID']

                    eventType = tweet['eventType']

                    postCategories = tweet['postCategories']

                    if eventID not in JSON_DATA:
                        if eventID not in missing:
                            missing.append(eventID)
                    else:
                        if eventID not in present:
                            present.append(eventID)

                        found = False
                        for searched_tweet in JSON_DATA[eventID]:
                            # We search for the tweet in the json data
                            # Many of the tweets in the labels file
                            # do not have the tweet text
                            if 'id_str' not in searched_tweet:
                                id_str = str(searched_tweet['id'])
                            else:
                                id_str = searched_tweet['id_str']
                            if postID == id_str:
                                found = True
                                if len(postCategories) > 0:
                                    if 'full_text' not in searched_tweet:
                                        tweet_text = searched_tweet['text']
                                    else:
                                        tweet_text = searched_tweet['full_text']
                                    tweet_text = tweet_text.replace(
                                        '\n', ' ').replace('\t', ' ')
                                    tweet_text = ' '.join(
                                        tweet_text.split()).strip()
                                    TWEET_DATA.append({'tweet_id': postID, 'first_label': postCategories[0], 'all_labels': postCategories,
                                                       'tweet_text': tweet_text, 'event_name': eventID,
                                                       'postPriority': tweet['postPriority'], 'eventType': tweet['eventType']})
                                    unique_labels += postCategories
                                    break
                                else:
                                    print(
                                        'Tweet without labels:', eventID, eventType, tweet)
                        if found == False:
                            print(
                                'Tweet not found:', eventID, eventType, tweet)

        print('Datasets in training:', present)
        print('Missing datasets:', missing)

        print('Datasets not in train:', set(not_in_train))

        unique_labels = set(unique_labels)

        print('Unique labels:', unique_labels)

    #    from sklearn.model_selection import train_test_split

        data = pd.DataFrame.from_dict(TWEET_DATA)

    #    import pdb;pdb.set_trace()

        data['event_number'] = data['event_name'].apply(
            lambda x: int(x.split('-')[-1]))
        print(data.head())

        # 14 event types for testing, the rest for training
        mask = data['event_number'] < 60

        X_train = data[mask]
        X_test = data[~mask]

    #    X_train, X_test, y_train, y_test = train_test_split(data, data['first_label'], test_size=0.2, random_state=2021)
    #    X_test, X_val, y_test, y_val = train_test_split(X_test, y_test, test_size=0.5, random_state=2021)

        # Test data format
        {"topic": "TRECIS-CTIT-H-Test-022",
         "runtag": "myrun",
         "tweet_id": "991855886363541507",
         "priority": 0.67,
         "info_type_scores": [0.2, 0.31, 0.1, 0.7, 0.0, ...],
         "info_type_labels": [0, 0, 0, 1, 0, ...]}

        del TWEET_DATA
        del JSON_DATA

        LIST_LABELS = ["CallToAction-Donations", "CallToAction-MovePeople",
                       "CallToAction-Volunteer", "Other-Advice", "Other-ContextualInformation",
                       "Other-Discussion", "Other-Irrelevant", "Other-Sentiment",
                       "Report-CleanUp", "Report-EmergingThreats", "Report-Factoid",
                       "Report-FirstPartyObservation", "Report-Hashtags", "Report-Location",
                       "Report-MultimediaShare", "Report-News", "Report-NewSubEvent",
                       "Report-Official", "Report-OriginalEvent", "Report-ServiceAvailable",
                       "Report-ThirdPartyObservation", "Report-Weather", "Request-GoodsServices",
                       "Request-InformationWanted", "Request-SearchAndRescue"]

        def search_in_labels(label):
            for idx, existing_label in enumerate(LIST_LABELS):
                if label in existing_label:
                    return idx

        countHighCriticalImport = 0
        countLowMediumImport = 0
        PRIORITY_MAP = {"Critical": 1.0,
                        "High": 0.75,
                        "Medium": 0.5,
                        "Low": 0.25}

        X_test_annotated = []
        X_test_labels = []

        for _, row in X_test.iterrows():
            labels = [0] * len(LIST_LABELS)  # initiate list with empty labels
            for label in row['all_labels']:
                # search label MovePeople --> CallToAction-MovePeople --> 1
                idx = search_in_labels(label)
                labels[idx] = 1

            priority = row['postPriority']
            if priority == "High" or priority == "Critical":
                countHighCriticalImport = countHighCriticalImport + 1

            if priority == "Low" or priority == "Medium":
                countLowMediumImport = countLowMediumImport + 1

            X_test_annotated.append({"topic": row['event_name'],
                                     "runtag": "test_run",
                                     "tweet_id": row['tweet_id'],
                                     "priority": PRIORITY_MAP[priority],
                                     "info_type_scores": [1.0] * len(LIST_LABELS),
                                     "info_type_labels": labels})
            X_test_labels.append({
                "eventID": row['event_name'],
                "eventType": row["eventType"],
                "postID": row['tweet_id'],
                "postCategories": row['all_labels'],
                "postPriority": priority,
                "postText": row['tweet_text']
            })

        print('Saving datasets (train/test/valid).')
        data.to_csv(os.path.join(out_dir, 'all.csv'), index=False)
        X_train.to_csv(os.path.join(out_dir, 'train.csv'), index=False)
    #    X_val.to_csv(os.path.join(out_dir, 'dev.csv'), index=False)
        X_test.to_csv(os.path.join(out_dir, 'test.csv'), index=False)

        with open(os.path.join(out_dir, 'test.json'), 'w', encoding='utf-8') as f:
            json.dump(X_test_annotated, f, ensure_ascii=False, indent=2)
        with open(os.path.join(out_dir, 'test.labels.json'), 'w', encoding='utf-8') as f:
            json.dump(X_test_labels, f, ensure_ascii=False, indent=4)

    if test:

        # Prepare test-data
        # data.plot(kind='bar')
        #    JSON_DATA = {}
        TWEET_DATA = []
        for path, directories, files in os.walk(data_test_dir):
            for file in files:
                if file.endswith('json'):
                    print('Reading %s' % os.path.join(path, file))
                    title = file.split('.')[-2]
    #                if title not in JSON_DATA:
    #                    JSON_DATA[title] = []
    #                eventID = title
                    with open(os.path.join(path, file), 'r') as f:
                        tweets = []
                        for line in f.readlines():
                            #                        tweets.append(json.loads(line))
                            tweet = json.loads(line)
                            if 'id_str' not in tweet:
                                id_str = str(tweet['id'])
                            else:
                                id_str = tweet['id_str']
                            if 'full_text' not in tweet:
                                tweet_text = tweet['text']
                            else:
                                tweet_text = tweet['full_text']

                            eventID = tweet['topic']
                            tweet_text = tweet_text.replace(
                                '\n', ' ').replace('\t', ' ')
                            tweet_text = ' '.join(tweet_text.split()).strip()
                            TWEET_DATA.append({'tweet_id': id_str, 'first_label': 'NONE', 'all_labels': [],
                                               'tweet_text': tweet_text, 'event_name': eventID,
                                               'postPriority': 'NONE', 'eventType': tweet['eventType']})
                            json_data = {}

    #                    JSON_DATA[title] += tweets

    #    for eventID in JSON_DATA.keys():

        data_test = pd.DataFrame.from_dict(TWEET_DATA)
        data_test.to_csv(
            os.path.join(
                out_dir,
                'test_not_labeled.csv'),
            index=False)
