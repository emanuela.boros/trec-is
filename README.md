# trec-is

`python prepare_data.py --labels TRECIS-2018-2020B.json --topics TRECIS-2018-2020B.topics --jsons_train Task-A-2021-data-train --jsons_test Task-A-2021-data-test --out Task-A-2021-data-processed --train --test`

`--train` = generate the train and test data for experiments. The split is done by event type. 14 event types in test, the rest in train.

`--test` = generate the competition test data (not labeled)